#!/usr/bin/env python
"""
Script to analyse Apache HTTPD 2.2+ "combined" logs and provide statistics related to web traffic.
"""

import collections
import datetime
import math
import re
import sys

__author__ = "Robert Grizzell"
__credits__ = ["Wonderful members of StackOverflow"]

__license__ = "Apache License 2.0"
__version__ = "0.1a"
__maintainer__ = "Robert Grizzell"
__email__ = "robert@grizzell.me"
__status__ = "Development"


# The main function performs the necessary steps to open and parse the file.
def parse(file):

    # TODO: Convert these capture groups to "named capture groups". ex: (?P<name>[A-Z]+)
    # The regular expression used to split the Apache log entry into multiple parts.
    # Capture Group 1: Remote Host
    # Capture Group 2: User Identity (identd)
    # Capture Group 3: HTTP Auth Username
    # Capture Group 4: Time of Request
    # Capture Group 5: Request Method
    # Capture Group 6: Request URI
    # Capture Group 7: HTTP Protocol
    # Capture Group 8: HTTP Return Status Code
    # Capture Group 9: Size of Response (in bytes)
    # Capture Group 10: Referer Header
    # Capture Group 11: User-agent Header
    # Capture Group 12: Page Load Time (in microseconds)
    pattern = '^([0-9.]+)\s' \
              '([\w.-]+)\s' \
              '([\w.-]+)\s' \
              '\[(.+)\]\s' \
              '\"([\w]+)\s' \
              '((?:[^\"])+)\s' \
              '((?:[^\"])+)\"\s' \
              '(\d{3})\s(\d+|-)\s' \
              '\"((?:[^\"])+)\"\s' \
              '\"((?:[^\"])+)\"\s' \
              '(\d+)$'

    f = ''
    output = []

    # Exception handling for opening the log file. Print out different error messages based on the exception type.
    try:
        f = open(file, 'r')
    except IOError as e:
        print("I/O error({0}): {1}".format(e.errno, e.strerror))
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise

    for log_entry in f:
        # Using the regexp, find all of the data.
        result = re.findall(pattern, log_entry)[0]

        # TODO: Eliminate this unnecessary object once "named capture groups" are in place.
        # Generate the output object with a bunch of results. This object will be fairly large in size.
        output.append({
            'remote_host': result[0],
            'user_identd': result[1],
            'user_http_auth': result[2],
            'timestamp': result[3],
            'request_method': result[4],
            'request_uri': result[5],
            'http_protocol': result[6],
            'response_code': int(result[7]),
            'response_size': int(result[8]),
            'referer_header': result[9],
            'user_agent': result[10],
            'time_to_serve': int(result[11]),
        })

    return output

# This is the main entry point into the file if it's run as a script.
if __name__ == "__main__":

    # Determine if an argument was passed along the command line and ask for one if it wasn't.
    if len(sys.argv) > 1:
        s = parse(sys.argv[1])
    else:
        print("Please specify the log file to parse.")
        s = parse(input("File: "))

    # Instantiate a bunch of objects.
    info = dict()
    timestamps = list()
    uri_list = list()
    visitor_list = list()
    load_time_list = list()
    error_list = list()
    data_list = list()

    # Prepare the data for processing. This is inefficient, but necessary.
    for line in s:

        # TODO: Find a way to parse the timezone field on Python >2.6
        # Convert the string into a DateTime object and add it into a list.
        timestamps.append(datetime.datetime.strptime(line['timestamp'], "%d/%b/%Y:%H:%M:%S %z"))

        # Simply append each log entry's fields into a new list.
        uri_list.append(line['request_uri'])
        visitor_list.append(line['remote_host'])
        load_time_list.append(line['time_to_serve'])
        data_list.append(line['response_size'])

        # If the response code is 4xx or 5xx, count it as an error in the list.
        if line['response_code'] in range(400, 599):
            error_list.append(line['response_code'])

    # Find the total number of objects in the list. In this case, it will match the number of log lines.
    info['num_of_lines'] = str(len(s))

    # Take the oldest and newest timestamps, compute a time delta, then convert that into a human-readable string.
    info['log_duration'] = str(
        datetime.timedelta(
            seconds=datetime.timedelta.total_seconds(
                max(timestamps) - min(timestamps)
            )
        )
    )

    # Use a collections counter to parse through the list and find the most common URI. Since the first result from the
    #  Tuple is all that is required, specify the indexes too.
    info['most_requested_page'] = str(
        collections.Counter(uri_list).most_common(1)[0][0]
    )
    info['most_frequent_visitor'] = str(
        collections.Counter(visitor_list).most_common(1)[0][0]
    )

    # Simple min/max search functions on the list.
    info['min_page_load'] = str(min(load_time_list))
    info['max_page_load'] = str(max(load_time_list))

    # Compute a simple average, and round up for clarity.
    info['avg_page_load'] = str(
        math.ceil(
            sum(load_time_list) / len(load_time_list)
        )
    )

    # Find the total number of objects in the list. In this case, it will match the errors in the logs.
    info['num_of_errors'] = str(len(error_list))

    # Convert data from bytes to megabytes.
    info['total_data_transferred'] = str(
        math.ceil(
            sum(data_list)/1024/1024
        )
    )

    # Print out information about the log file.
    print("Number of lines parsed: {}".format(info['num_of_lines']))
    print("Duration of log file: {}\n".format(info['log_duration']))

    # Print the most frequent page request and visitor.
    print("Most requested page: {}".format(info['most_requested_page']))
    print("Most frequent visitor: {}\n".format(info['most_frequent_visitor']))

    # Print out information about page load times.
    print("Min page load time: {} ms".format(info['min_page_load']))
    print("Average page load time: {} ms".format(info['avg_page_load']))
    print("Max page load time: {} ms\n".format(info['max_page_load']))

    # Print out additional information about the traffic.
    print("Number of errors: {}".format(info['num_of_errors']))
    print("Total data transferred: {} MB".format(info['total_data_transferred']))
