# apache-parse
Script to analyse Apache HTTPD 2.2+ "combined" logs and provide statistics related to web traffic.

### Features

The script was coded to allow some degree of versatile use in other Python scripts. Calling _apachelog.parse(logfile)_ will return a list of dictionary objects corresponding to each processed line. This object can then be manipulated as you see fit.

## Requirements

Python 3.4+

## Known Issues and Areas for Improvements

+ In Python 2.7 and below, converting a string into a DateTime object using [datetime.strptime()](https://docs.python.org/2/library/datetime.html#datetime.datetime.strptime) does not allow the use of the timezone format typically found in Apache logs.
+ The regular expression and process used for parsing the lines has room for improved efficiency.
+ No unit tests are currently included in this script.

## Code Example
    $ python3 ~/apache-parse/apachelog.py
    Please specify the log file to parse.
    File: /var/log/apache2/access_log.1

    Number of lines parsed: 43378
    Duration of log file: 2 days, 12:11:55

    Most requested page: /kms/my/home/
    Most frequent visitor: 37.17.24.145

    Min page load time: 538 ms
    Average page load time: 2504 ms
    Max page load time: 4580 ms

    Number of errors: 2649
    Total data transferred: 207 MB

## License

This project is licensed under the Apache License 2.0.